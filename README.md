# Projet pour éprouver la solution Postal


Ce projet a été intialié dans le cadre d'un travail commun avec la DINUM (contact : Julien DAUPHANT) pour construire une solution d'envoi de mails à destination des startups d'Etat.
L'idée première est de tester le serveur d'envoi de mails OpenSource Postal afin de déterminer si ce serveur pourra être utilisé pour notre solution d'envoi de mails.


Ce projet permet de déployer via GitLab CI, le serveur SMTP Postal sur un serveur host. Le serveur Postal est conteneurisé avec Docker en utilisant Docker Compose pour gérer tous les conteneurs suivants :

*  conteneur Postal : serveur SMTP
*  conteneur MySQL : base de données MariaDB
*  conteneur RabbitMQ : message brokers RabbitMQ
*  conteneur NGINX : reverse proxy pour accéder à l'interface web Postal et l'api
*  conteneur Adminer : interface web d'accès à la base de données MySQL postal du serveur SMTP

# Comment vérifier l'état du serveur Postal


**Vérifier l' état des conteneurs**

Se connecter sur la VM puis taper la commande suivante :
```
docker container ls
```

**Vérifier le status des processus lancer par Postal**

* [Pour en savoir plus, consulter la doc Postal sur la description des processus](https://github.com/postalhq/postal/wiki/Processes)

Pour vérifier le status des processus, se connecter sur le serveur et se placer dans le répertoire /home/docker/baggy puis taper la commande suivante pour obtenir un prompt bash sur le conteneur :

```
docker-compose exec postal bash
```

Une fois, le prompt bash sur le conteneur obtenu, il suffit de taper la commande suivante :
```
postal status
```

**Consulter les logs**

Se connecter sur le serveur et se placer dans le répertoire /home/docker/baggy puis taper la commande suivante :
```
docker-compose logs **SERVICE_NAME_DOCKER**
```

# Accéder à l'interface web RabbitMQ Management

Il est possible d'accèder à l'interface web RabbitMQ Management ([https://postal.incubateur.net/rabbitmq/](https://postal.incubateur.net/rabbitmq/))

# Accéder à la base de données MySQL du serveur Postal

Il est possible d'accèder à la base de données via l'interface web Adminer ([https://postal.incubateur.net/adminer/](https://postal.incubateur.net/adminer/))

# Creer un user administrateur du service Postal


Se connecter sur le serveur et se placer dans le répertoire /home/docker/baggy puis taper la commande suivante pour obtenir un prompt bash sur le conteneur :
```
docker-compose exec postal bash
```

Ensuite, taper la commande suivante pour créer un user administrateur :
```
postal make-user
```

Vous pourrez ensuite accéder à l'interface d'administration depuis [https://postal.incubateur.net/login](https://postal.incubateur.net/login)


# Modifier la configuration du serveur Postal

* Modifier le fichier de configuration Postal présent dans ce projet GitLab sous **docker-compose/baggy-docker-compose/postal/config/local/postal.yml.j2**
* Lancer manuellement le job GitLab CI **generate-config-files** pour générer les nouveau fichier de conf avec p2cli pour set les variables d'environnement avec les variables GitLab CI du projet.
* Lancer manuellement le job GitLab CI **deploy** pour que la mise à jour de la configuration soit prise en compte sur le serveur

Ce fichier de configuration vient surcharger la configuration par défaut du serveur Postal **postal/config/postal.defaults.yml**

# Modifier la configuration Docker Compose

* Modifier le fichier de configuration Docker Compose présent dans ce projet GitLab sous **docker-compose/docker-compose.yml.j2**
* Lancer manuellement le job GitLab CI **generate-config-files** pour générer le nouveau fichier de conf avec p2cli pour set les variables d'environnement avec les variables GitLab CI du projet.
* Lancer manuellement le job GitLab CI **deploy** pour que la mise à jour de la configuration soit prise en compte sur le serveur

# Modifier la configuration du reverse proxy NGINX

* Modifier le fichier de configuration NGINX présent dans ce projet GitLab sous **docker-compose/baggy-docker-compose/nginx/nginx.conf**
* Lancer manuellement le job GitLab CI **deploy** pour que la mise à jour de la configuration soit prise en compte sur le serveur

# Modifier l'image Docker **incubateur-pe/postal**

* Modifier le fichier Dockerfile présent dans ce projet GitLab sous **customized-postal-image**
* Lancer manuellement le job GitLab CI **build-docker-image** pour build la nouvelle image et la deployer dans le container registry du GitLab
* Lancer manuellement le job GitLab CI **deploy** pour que la nouvelle image soit déployée sur le serveur

# Quelques docs utiles

* suivi des échanges avec Julien Dauphant de la DINUM : [https://pad.incubateur.net/GSsL19n0R1O7MPgrWkRKVg](https://pad.incubateur.net/GSsL19n0R1O7MPgrWkRKVg)
* doc officielle Postal : [https://postal.atech.media/](https://postal.atech.media/)
* [How to create a fully featured mail server using postal](https://www.howtoforge.com/tutorial/how-to-create-a-fully-featured-mail-server-using-postal/)
* RabbitMQ : [https://www.ionos.fr/digitalguide/sites-internet/developpement-web/rabbitmq/](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/rabbitmq/)


# Quelques articles intéressants sur le mailing

* [Serveur SMTP : définition et fonctionnement](https://www.ionos.fr/digitalguide/email/aspects-techniques/serveur-smtp/)
* [Best SMTP servers](https://www.171mails.com/2018/01/16/best-mta-email-marketer/)
* [Qu’est-ce que le Return-Path ?](https://fr.mailjet.com/blog/news/personnalisation-return-path/)
* [Protocoles SPF, DKIM et DMARC, pourquoi ?](https://fr.mailjet.com/blog/news/protocoles-spf-dkim-dmarc/)

# Quelques tools 

* [SPF check](https://mxtoolbox.com/spf.aspx)
* [How to test SMTP Server](https://blog.mailtrap.io/how-to-test-smtp-server/)
* [IP & Domain Reputation Center](https://talosintelligence.com/reputation_center)

# Comment déployer le serveur Postal from scratch

* Lancer manuellement le job GitLab CI **generate-config-files** pour générer les fichiers de configuration (nginx, postal) avec p2cli pour set les variables d'environnement avec les variables GitLab CI du projet.
* Lancer manuellement le job GitLab CI **deploy** pour le serveur Postal soit déployé sur la VM host
* Se connecter sur la VM et se placer dans /home/docker/baggy puis executer la commande
```
make init
```
* Redémarrer le service Docker postal en tapant la commande suivante :
```
docker-compose restart postal
```

# License

Ce projet est sous licence MIT. Une partie du code de ce projet est inspiré du code des projets suivants :
* [Baggy](https://github.com/StartupsPoleEmploi/baggy) 
* [CatDeployed/docker-postal](https://github.com/CatDeployed/docker-postal/)

