#!/bin/bash

## Replace values in spamassassin config file to enable spam checking

## Wait for MySQL to start up
echo "== Waiting for MySQL to start up =="
while ! mysqladmin ping -h mysql --silent; do
    sleep 0.5
done

## Run command
"$@"
